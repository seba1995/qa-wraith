To run test regression test:

Install the tooling needed http://bbc-news.github.io/wraith/ 
see the possible issues https://www.evernote.com/shard/s21/sh/8ced80e7-0c23-4e5e-8b6f-ee6babfc7038/5eb2b86ca059e0e654ee92b84df17c73

#BASE SCREENSHOTS
$ wraith history configs/history.yaml

#CURRENT VS BASE DIFF + GENERATOR
$ wraith latest configs/history.yaml

#PROD VS STAGING
$ wraith capture configs/capture.yaml

#Configurations:
this query param is send to avoid false positive clients on SEO
https://url/?wraith


configs/capture.yaml edit 
    domains (stagin vs prod) 
    path (pages to compare)
    screen_width (resolutions take in account)

configs/history.yaml
    paths:
        home:
            path: /url
        contacto:
            path: /url

#To see results
open shots/gallery.html too see files