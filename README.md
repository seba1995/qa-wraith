# QA-wraith

To run test regression test:

Install the tooling needed http://bbc-news.github.io/wraith/ 
see the possible issues https://www.evernote.com/shard/s21/sh/8ced80e7-0c23-4e5e-8b6f-ee6babfc7038/5eb2b86ca059e0e654ee92b84df17c73

#BASE SCREENSHOTS
$ wraith history configs/history.yaml

#CURRENT VS BASE DIFF + GENERATOR
$ wraith latest configs/history.yaml

#PROD VS STAGING
$ wraith capture configs/capture.yaml

#Configurations:
this query param is send to avoid false positive clients on SEO
https://url/?wraith


configs/capture.yaml edit 
    domains (stagin vs prod) 
    path (pages to compare)
    screen_width (resolutions take in account)

configs/history.yaml
    paths:
        home:
            path: /url
        contacto:
            path: /url

#To see results
open shots/gallery.html too see files

#AUTOMATE WITH JENKINS
AUTOMATE IT!
That's all well and good, but who wants to sit there and run this manually all the time? Let's make it run nightly and dump the gallery into a place that the whole team can access. For this part, you'll obviously need to have some method of running commands nightly - Jenkins, crontab, whatever. Also, assuming we're talking about the kind of site that gets out of sync, meaning that new content is created often on production which needs to be synced back to sandbox or staging periodically, I recommend setting it up so that Wraith runs immediately after this DB sync happens. This way, you minimize the risk of false positives due to out of sync data. With all of that out of the way, you just need to schedule the running of a set of commands that will run Wraith and copy the directory it generates into a directory that is available on the web to your team. For example, let's say that you're working on http://yoursite.com. You could create a http://wraith.yoursite.com subdomain which points to /var/www/wraith, and then run this nightly:

[sh] # Go to wherever your configs exist
cd /path/to/your/configs

# Run Wraith and ignore any error code it may return
wraith capture configs/config.yaml || true

# Move the results onto the test site
rm -rf /var/www/wraith/*
mv /shots/* /var/www/wraith [/sh]
Pretty simple. The only gotcha is the "|| true" part. If Wraith spots any errors, meaning it finds a path that has a differences percentage that is above the threshold you set, then it will return an error code. If you're using Jenkins, then that error code would kill your build. So we just need to ignore it. Run that little chunk of code every night, and every morning when you start work, you'll have a fresh visual regression test of the previous day's worth of work to check out.